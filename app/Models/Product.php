<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable = ['id', 'name'];
	
    public function category()
	{
		return $this->belongsTo('App\Models\Category')->select('id', 'name');
	}
}
