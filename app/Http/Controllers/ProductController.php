<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;

class ProductController extends Controller
{
	public function __construct()
	{
        $this->middleware('auth');
	}
	
	// View Products
	public function index()
	{
		return view('/product/view-products');
	}
	
    // Create Product
	public function create()
	{
		$categories = Category::select('id', 'name')->get();
		return view('/product/create-product', compact('categories'));
	}
	
	// Edit Product
	public function edit($product_id)
	{
		$product = Product::with('category')->findOrFail($product_id);
		$categories = Category::select('id', 'name')->get();
		return view('/product/edit-product', compact('product', 'categories'));
	}
}
