<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Response;

class ProductController extends Controller
{
	public $request;
	public function __construct(Request $request)
	{
		$this->request = $request;
	}
	
	public function getAllProducts()
	{
		$products = Product::with('category')->get();
		$response = ['products' => $products];
		
		if($products->isEmpty())
			$status_code = 404;
		else
			$status_code = 200;
		
		return Response::json($response, $status_code);
	}
	
	public function createProduct()
	{
		return $this->storeProduct();
	}
	
	public function editProduct($product_id)
	{
		$product = Product::find($product_id);
		if($product)
			return $this->storeProduct($product);
		
		return Response::json(['message' => 'Product does not exist.'], 404);
	}
	
	private function storeProduct($product=null)
	{
		$this->request->validate([
			'name' => 'required',
			'category_id' => 'required|numeric|min:1',
			'price' => 'required|numeric|min:1',
		]);
		
		if($product === null)
			$product = new Product();
		
		$product->name = $this->request->name;
		$product->category_id = $this->request->category_id;
		$product->price = $this->request->price;

		if(!$product->save()) {
			$status_code = 500;
			$response = ['message' => 'Something went wrong, please try again later.'];
		}
		else {
			$status_code = 201;
			$response = ['message' => 'Request has been successfully processed.'];
		}
		
		return Response::json($response, $status_code);
	}
	
}
