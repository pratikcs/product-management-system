<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'ProductController@index')->name('view-products');

Route::group(['prefix' => '/product'], function() {
	Route::get('/create', 'ProductController@create')->name('create-product');
	Route::get('/edit/{product_id}', 'ProductController@edit')->name('edit-product');
});