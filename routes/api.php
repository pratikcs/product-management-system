<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('products', 'API\ProductController@getAllProducts')->name('get-products-api');
Route::post('create-product', 'API\ProductController@createProduct')->name('create-product-api');
Route::post('edit-product/{product_id}', 'API\ProductController@editProduct')->name('edit-product-api');
