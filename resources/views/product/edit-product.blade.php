@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
			<div id="response-container" class="alert" style="display:none"></div>
            <div class="card">
                <div class="card-header">Edit Product</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('edit-product-api', $product->id) }}" onsubmit="formSubmission(event, this)">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Product Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" value="{{ $product->name }}" class="form-control" name="name" placeholder="" required autocomplete="name" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="category" class="col-md-4 col-form-label text-md-right">Product Category</label>

                            <div class="col-md-6">
								<select class="form-control" name="category_id">
									<option value="">-----Select Category-----</option>
									@foreach($categories as $category)
										<option value="{{ $category->id }}" @if($category->id === $product->category->id) selected @endif>{{ $category->name }}</option>
									@endforeach
								</select>
                            </div>
                        </div>

						
						<div class="form-group row">
                            <label for="price" class="col-md-4 col-form-label text-md-right">Product Price</label>

                            <div class="col-md-6">
                                <input id="price" type="number" min="1" class="form-control" name="price" value="{{ $product->price }}" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <input type="submit" class="btn btn-success" value="Update">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
