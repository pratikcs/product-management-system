@extends('layouts.app')
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
<link rel="stylesheet" href="{{ asset('css/style.css') }}">

@section('content')
<div class="container">
  <h2>Product Listing</h2>
  <ul class="responsive-table" id="product-container">
    <li class="table-header">
      <div class="col col-1">Id</div>
      <div class="col col-2">Product Name</div>
      <div class="col col-3">Product Category</div>
      <div class="col col-4">Product Price</div>
      <div class="col col-5">Action</div>
    </li>
  </ul>
  <h3 id="product-placeholder" class="text-center">Products will be loaded here, please wait...</h3>
</div>
@endsection

@section('scripts')
	<script>
		var getProductsUrl = "{{ route('get-products-api') }}";
		var editProductUrl = "{{ url('product/edit') }}";
		getProducts(getProductsUrl, editProductUrl); 
	</script>
@endsection