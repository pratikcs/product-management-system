function getProducts(getProductUrl, editProductUrl) {
	var xhr = new XMLHttpRequest();
	xhr.open('GET', getProductUrl);
	xhr.setRequestHeader("Accept", "application/json");
	xhr.onload = function() {
		var productPlaceholder = document.getElementById('product-placeholder');
		if(this.status === 200) {
			productPlaceholder.outerHTML = '';
			var productContainer = document.getElementById('product-container');
			var products = JSON.parse(xhr.responseText).products;
			products.forEach(function(product) {
				productContainer.innerHTML += ' \
					<li class="table-row"> \
						<div class="col col-1" data-label="Id">' +product.id+ '</div> \
						<div class="col col-2" data-label="Product Name">' +product.name+ '</div> \
						<div class="col col-3" data-label="Category">' +product.category.name+ '</div> \
						<div class="col col-4" data-label="Price">' +product.price+ '</div> \
						<div class="col col-5" data-label="Price"> \
							<a href=' +editProductUrl+ '/' +product.id+ '>Update Details</a> \
						</div> \
					</li> \
				';
			});
		}
		else {
			productPlaceholder.innerHTML = 'No products to load.';
		}
	}
	xhr.send();
}


function formSubmission(event, form) {
	event.preventDefault();
	var formData = new FormData(form);
	var responseContainer = document.getElementById('response-container');
	responseContainer.removeAttribute('class');
	responseContainer.innerHTML = '';
	var xhr = new XMLHttpRequest();
	xhr.open('POST', form.action);
	xhr.setRequestHeader("Accept", "application/json");
	xhr.onload = function() {
		responseContainer.style.display = 'block';
		if(this.status === 201) {
			var responseData = JSON.parse(xhr.responseText);
			responseContainer.classList.add('alert', 'alert-success');
			responseContainer.innerHTML = responseData.message;
		}
		else if(this.status === 422) {
			responseContainer.classList.add('alert', 'alert-danger');
			var errors = JSON.parse(xhr.responseText).errors;
			responseContainer.innerHTML = '<ul>';
			for(var error in errors) {
				var errorMessages = errors[error];
				errorMessages.forEach(function(errorMessage) {
					responseContainer.innerHTML += '<li>' +errorMessage+ '</li>';
				});
			}
			responseContainer.innerHTML += '</ul>';
		}
		else {
			console.log(xhr.responseText);
		}
	}
	xhr.send(formData);
}
